package com.Feathered.Beauties.birds.nature.app

data class BeautiesEntity (
    val argBirdsTitle: String?,
    val argBirdsPicture: Int?,
    val argBirdsDesc: String
)