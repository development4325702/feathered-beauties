package com.Feathered.Beauties.birds.nature.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.Feathered.Beauties.birds.nature.app.databinding.ActivityMainBinding

class MainScreen : AppCompatActivity() {

    private lateinit var mainBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mainBinding.root)
        setBeautiesFullScreen()

        mainBinding.beautifulBirdsBtn.setOnClickListener {
            val intent = Intent(this@MainScreen, BeautyBirdsScreen::class.java)
            startActivity(intent)
        }

        mainBinding.exitBtn.setOnClickListener {
            finish()
        }

    }

    private fun setBeautiesFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}