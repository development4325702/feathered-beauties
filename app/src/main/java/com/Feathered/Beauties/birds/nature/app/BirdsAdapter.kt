package com.Feathered.Beauties.birds.nature.app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


class BirdsAdapter(private val players: ArrayList<BeautiesEntity>) :
    RecyclerView.Adapter<BirdsAdapter.ViewHolder>() {

    var birdsPosition = 0

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val playersImg: ImageView = itemView.findViewById(R.id.birdsImage)
        val playersName: TextView = itemView.findViewById(R.id.birdsName)
        val playersDesc: TextView = itemView.findViewById(R.id.birdsDesc)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.birds_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val playersList = players[position]
        playersList.argBirdsPicture?.let { holder.playersImg.setImageResource(it) }
        holder.playersDesc.text = playersList.argBirdsDesc
        holder.playersName.text = playersList.argBirdsTitle
        birdsPosition = holder.adapterPosition
    }

    override fun getItemCount(): Int = players.size
}