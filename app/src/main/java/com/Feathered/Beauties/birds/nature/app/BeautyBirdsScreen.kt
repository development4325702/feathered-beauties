package com.Feathered.Beauties.birds.nature.app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.Feathered.Beauties.birds.nature.app.databinding.ActivityBeautyBirdsBinding
import com.Feathered.Beauties.birds.nature.app.databinding.ActivityMainBinding

class BeautyBirdsScreen : AppCompatActivity() {

    private lateinit var beautyBirdsBinding: ActivityBeautyBirdsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        beautyBirdsBinding = ActivityBeautyBirdsBinding.inflate(layoutInflater)
        setContentView(beautyBirdsBinding.root)
        setBeautiesFullScreen()

        beautyBirdsBinding.backBtn.setOnClickListener {
            onBackPressed()
        }

        val playersData = ArrayList<BeautiesEntity>()
        playersData.add(
            BeautiesEntity(
                "Hyacinth macaw",
                R.drawable.birds1,
                "The hyacinth macaw is a bird from the parrot family. It differs from other similar species in the beautiful blue color of the plumage and larger size. He has a loud, sharp voice that is carried over long distances. Its habitat is the territories of Brazil and Bolivia.\n" +
                        "He was on the verge of extinction due to hunting, capture, tree cutting and forest fires. Listed in the Red Book as a rare species."
            )
        )

        playersData.add(
            BeautiesEntity(
                "Curly Arasari",
                R.drawable.birds2,
                "Curly Arasari belongs to the family of toucans of the woodpecker. It is found in the center of Brazil, as well as in Bolivia, Ecuador and Peru. The bird has a bright appearance: multicolored feathers, a beak, and on the head bizarre feathers in the form of curls. The length of the arasari body is about 40 cm, and the weight does not exceed 280 grams.\n" +
                        "Their attitude to the offspring is interesting. Both females and males are engaged in education and feeding."
            )
        )

        playersData.add(
            BeautiesEntity(
                "Mandarin duck",
                R.drawable.birds3,
                "The Mandarin is a small bird of the duck family. Males of this species have a very original color. Their feathers are decorated with all the colors of the rainbow. They weigh an average of 0.5-0.6 kg, the body length is a little more than 20 cm. The bird is migratory. It nests in Sakhalin, in the Amur region, in the Primorsky Territory, and winters in Japan and China.\n" +
                        "Mandarin - listed in the Red Book of Russia as a rare species."
            )
        )

        playersData.add(
            BeautiesEntity(
                "The crowned crane",
                R.drawable.birds4,
                "The crowned crane is a sedentary bird that lives in southern Africa. She has a yellow tuss on her head, which is why the bird has such a name. Her height reaches one meter, and her weight is about 5 kg. The crane has an elongated back finger. This allows you to catch the branch well. By the way, the crowned crane sleeps on the trees. The diet of the crane is very diverse: from plant seeds to small vertebrates."
            )
        )

        playersData.add(
            BeautiesEntity(
                "Spukha",
                R.drawable.birds5,
                "The common owl belongs to the order of owls and is a predator. The basis of its nutrition is rodents. The front disc of this bird resembles a heart in shape. This distinguishes it from other species. The bird is from 32 cm to 39 cm long. The weight varies depending on the individual and reaches a maximum of 700 g.\n" +
                        "The barn hound is widespread everywhere except for cold northern places and highlands (due to the inability to accumulate subcutaneous fat)."
            )
        )

        playersData.add(
            BeautiesEntity(
                "Quezal",
                R.drawable.birds6,
                "Kvezal is a small (its body length does not exceed 35 cm) and an incredibly beautiful bird with a very long tail and motley plumage. In males, it is the most catchy: bright green on the body, and red on the abdomen.\n" +
                        "The quesal lives in southern Mexico, as well as in Panama and Guatemala. Prefers to settle on the upper parts of the trees. This species is on the verge of extinction and is listed in the International Red Book."
            )
        )

        playersData.add(
            BeautiesEntity(
                "Karolinska duck",
                R.drawable.birds7,
                "The Carolin duck from the duck family is relatively small: about half a meter long. The slees have a bright color with clear borders of colors. The plumage contains mainly green, brown and red colors. Females are not so brightly colored, their color is brown with the addition of green. The ducks of this species have a trest on their heads and a long triangular tail.\n" +
                        "Carolinian ducks are common in the waters of North America. They also live in European countries, where they were imported on purpose."
            )
        )

        playersData.add(
            BeautiesEntity(
                "The crowned pigeon",
                R.drawable.birds8,
                "The crowned pigeon is a very bright representative of the pigeon family. It is distinguished by an unusual blue color and a very lush khoholok. The length of the pigeon's body does not exceed 80 cm, and the weight does not exceed 2.5 kg. These birds are very slow on the ground and during flight. Representatives of this species eat exclusively plant food: seeds, berries and fruits."
            )
        )

        playersData.add(
            BeautiesEntity(
                "Whistle",
                R.drawable.birds9,
                "The whistle is a bird from the family of the svirstel order of sparrows. It has a pinkish-gray color, yellow and white spots on the wings and a reddish trest. Body weight varies between 60-70 g. The length is from 18 to 23 cm. The whistle has a beautiful voice, which sounds similar to the sounds of a flute.\n" +
                        "This unusual species lives in the taiga and forest tundra forests of the Northern Hemisphere of the planet."
            )
        )

        playersData.add(
            BeautiesEntity(
                "Blue jay",
                R.drawable.birds10,
                "The blue jay is a small songbird of the Vranov family. Its size is about 30 cm, the weight does not exceed 100 grams. A characteristic feature of the appearance of the jay is the blue color of the plumage. In addition to blue, there are white and black colors on the wings and tail. There is a crest on the bird's head and a black necklace pattern on the neck."
            )
        )

        playersData.add(
            BeautiesEntity(
                "King kister",
                R.drawable.birds11,
                "The kisper is a small bird weighing only from 25 to 45 g. He has a big head and beak, and his wings, tail and legs are short. Bright plumage is inherent in both males and females of this species. The birds have a green-blue color on the top and a red one at the bottom.\n" +
                        "The habitat of the kingpher is extensive. It is found in Scandinavia, Russia, Ukraine, Belarus, Kazakhstan, Indonesia, New Guinea, etc."
            )
        )

        playersData.add(
            BeautiesEntity(
                "Flamingo",
                R.drawable.birds12,
                "Flamingo is a genus of birds of the flaming family. They have long legs like many near-water birds, their neck is long and flexible, a large curved beak to look for food in water and slud. The color of their plumage varies from white to rich pink and red depending on the species."
            )
        )

        playersData.add(
            BeautiesEntity(
                "Golden pheasant",
                R.drawable.birds13,
                "The golden pheasant is a member of the pheasant family. Males of this species have very colorful plumage. The feathers are yellow on the back and bright red on the chest and abdomen. The feathers are blue on the shoulders. The head is decorated with a yellow cracked trest. Females look more modest, their plumage is gray-brown."
            )
        )

        playersData.add(
            BeautiesEntity(
                "Golden pheasant",
                R.drawable.birds14,
                "The white-backed lori belongs to the parrot family. His back and wings are olive brown, his abdomen and beak are red, and his head is yellow-orange. And the lori got its name because of the white color of the lower back, which is clearly visible with the wings spread out. White-backed loris inhabit New Guinea and neighboring islands."
            )
        )

        beautyBirdsBinding.birdsRecycler.layoutManager =
            LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)

        val adapter = BirdsAdapter(playersData)
        beautyBirdsBinding.birdsRecycler.adapter = adapter

        beautyBirdsBinding.nextBtn.setOnClickListener {
            beautyBirdsBinding.birdsRecycler.postDelayed(Runnable {
                beautyBirdsBinding.birdsRecycler.smoothScrollToPosition(adapter.birdsPosition + 0)
            }, 200)
            adapter.notifyDataSetChanged()
        }
    }

    private fun setBeautiesFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}