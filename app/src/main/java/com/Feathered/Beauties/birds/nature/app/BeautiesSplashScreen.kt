package com.Feathered.Beauties.birds.nature.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.Feathered.Beauties.birds.nature.app.databinding.ActivityBeautiesSplashScreenBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class BeautiesSplashScreen : AppCompatActivity() {

    private lateinit var beautiesSplashScreenBinding: ActivityBeautiesSplashScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        beautiesSplashScreenBinding = ActivityBeautiesSplashScreenBinding.inflate(layoutInflater)
        setContentView(beautiesSplashScreenBinding.root)
        setBeautiesFullScreen()

        GlobalScope.launch {
            delay(2500)
            val intent = Intent(this@BeautiesSplashScreen, MainScreen::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun setBeautiesFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}